<script>
  import Code from "$lib/ui/CodeBlock.svelte";
  import CC from "$lib/ui/CodeInline.svelte";
  import Card from "$lib/ui/Card.svelte";
  import SVG from "$lib/ui/SVG.svelte";
  import Ex_1 from "./exercises/ex_1.svelte";
  import Ex_2 from "./exercises/ex_2.svelte";
  import Ex_3 from "./exercises/ex_3.svelte";
  import Ex_4 from "./exercises/ex_4.svelte";
  import Ex_5 from "./exercises/ex_5.svelte";
  import Ex_6 from "./exercises/ex_6.svelte";
  import Ex_7 from "./exercises/ex_7.svelte";
</script>

<h1>Week 6</h1>
<p>
  For this week's exercises, we will cover how to build custom visualizations
  using Svelte. We will show some key <a
    target="_blank"
    class="link-secondary"
    href="https://d3js.org/">d3.js</a
  >
  functionalities and explain how to go from JavaScript objects to elements on the
  DOM. You will use
  <a
    target="_blank"
    class="link-secondary"
    href="https://www.w3schools.com/graphics/svg_intro.asp">SVG</a
  >
  as plotting back-end and experiment with
  <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-scale">scales</a
  >,
  <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-scale-chromatic">colors</a
  >, and
  <a target="_blank" class="link-secondary" href="https://github.com/d3/d3-axis"
    >axes</a
  > to build a static version of the famous Gapminder visualization by Hans Rosling.
</p>

<h5>Exercise 1 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the file <CC
    code="src/routes/week_6/exercises/ex_1.svelte"
  />. Any changes you make to that file should show up below.
</p>
<p>
  Scalable Vector Graphics (SVG) is an XML based specification for constructing
  graphics. It describes images as a combination of graphical primitives, like
  circles, rectangles, and lines. SVG is very usefull for custom visualizations,
  as we can just tell the browser what to show where, rather than having to deal
  with low-level graphics APIs. The only downside of SVG for our use is that it
  can get slow when you are dealing with many data points. In that case you
  should consider using a <a
    target="_blank"
    class="link-secondary"
    href="https://www.w3schools.com/html/html5_canvas.asp">Canvas</a
  > instead.
</p>
<p>
  Below is a basic example of an SVG element for a static visualisation. It is
  important to understand the coordinate system of an SVG element, as that is
  central to plotting the data where you want it. By default, the origin of an
  SVG element is at the top-left: the x-axis points to the right and the y-axis
  points to the bottom. We often want to reserve some space for axes and
  legends. In the example, we defined the <CC code="margin" /> variable which specifies
  how much space should be reserved on each side. The margins are coloured dark-blue
  while the content-area is light-blue. As you can see, the content-area's origin
  has moved away from the SVG element's origin. In addition, its width and height
  have shrunk. The <CC code="innerWidth" /> and
  <CC code="innerHeight" /> variables compute the new size of the content-area and
  we used a grouping (<CC code="g" />) tag that specifies the translation of the
  content-area's origin. This means that, when you are mapping data to the
  content-area, the x- and y-coordinates range from 0 to <CC
    code="innerWidth"
  />
  and
  <CC code="innerHeight" />, respectively.
</p>
<div class="d-flex flex-row justify-content-center">
  <Code
    language="svelte"
    code={`<\script> 
  const width = 436;
  const height = 300;
  const margin = { top: 10, right: 10, bottom: 50, left: 50 };
  const innerWidth = width - margin.left - margin.right;
  const innerHeight = height - margin.top - margin.bottom;
</\script>

<\svg {width} {height}>
  <\g transform='translate({margin.left}, {margin.top})'>
      ...
  </\g>
</\svg>
`}
  />
  <SVG />
</div>
<p>
  The two most useful SVG elements for data visualization are the circle (<CC
    language="svelte"
    code="<circle cx=0 cy=0 r=10 />"
  />) and rectangle (<CC
    language="svelte"
    code="<rect x=0 y=0 width=10 height=10 />"
  />). You can style both using CSS or by specifying attributes on the element.
  Common styling attributes include the fill, stroke, and opacity.
</p>
<p>
  Place a yellow rectangle with a width and height of 40 pixels so that it is
  centered in the content-area. Now, add a skyblue circle with a radius of 20
  pixels on the bottom-right corner of the rectangle.
</p>
<Card><Ex_1 /></Card>

<h5>Exercise 2 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the file <CC
    code="src/routes/week_6/exercises/ex_2.svelte"
  />. Any changes you make to that file should show up below.
</p>
<p>
  Sometimes, your visualization should scale to the space available to it on a
  web-page, but you may not know how much that is ahead of time. One way to
  resolve this issue is to use Svelte's reactivity (which we will cover next
  week) to observe the size of the SVG element on the page and update your
  values accordingly. Another approach is using SVG's <CC code="viewBox" /> attribute.
  The <CC code="viewBox" /> allows you to specify the SVG's coordinate system irrespective
  of the actual SVG element's size. It takes 4 arguments, namely the minimum x value,
  the minimum y value, the width, and the height of the coordinate space you want
  to use: <CC language="svelte" code="<svg viewBox='0 0 100 100'></svg>" />.
</p>

<p>
  Change the SVG element to use a <CC code="viewBox" /> instead of specifying the
  width and height and update the <CC code="width" /> and <CC code="height" /> values
  so that the size of circle is similar to the one in Exercise 1.
</p>
<Card><Ex_2 /></Card>

<h5>Exercise 3 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the file <CC
    code="src/routes/week_6/exercises/ex_3.svelte"
  />. Any changes you make to that file should show up below.
</p>
<p>
  Scales map values from one dimension onto values in another dimension.
  Typically, we need scales to map some attribute of the data onto the x- and
  y-coordinates of our visualization. The possible range of values we want to
  map onto the coordinates is called the <em>domain</em>. The possible range of
  values we want to map those values to is called the <em>range</em>. So, the
  data provides the domain, and the coordinates of the SVG provide the range.
  You can also use scales to map values onto color rather than space, but that
  is for another exercise.
</p>
<p>
  d3.js provides several very useful scale functions in the <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-scale">d3-scale</a
  > module. They all follow the same pattern. The function you import is used to
  construct a scale:
</p>
<Code
  language="javascript"
  code={`import { scaleLinear } from 'd3-scale';
const scale = scaleLinear();
`}
/>
<p>
  Once you have constructed the scale, it acts as a function that takes a value
  of the <em>domain</em> and returns a value in the <em>range</em>. It also has
  helper functions you can use to specify the minimum and maximum value of the
  <em>domain</em> and <em>range</em>. If you do not specify any values, both the
  <em>domain</em> and the <em>range</em> lie between 0 and 1, which is not useful
  for us!
</p>
<p>
  Plot the values in the <CC code="values" /> array as circles with a radius of 10
  pixels centered on the y-axis, where the value is mapped to the x-coordinate using
  a <strong>logarithmic</strong> scale.
  <em>
    Hint: if it does not look like you expect, what is the logarithm of zero?
  </em>
</p>
<Card><Ex_3 /></Card>

<h5>Exercise 4 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the file <CC
    code="src/routes/week_6/exercises/ex_4.svelte"
  />. Any changes you make to that file should show up below.
</p>
<p>
  Now lets add an axis to the previous exercise so that we can read the
  x-coordinates of the circles. You will need to import <CC code="axisBottom" />
  from the
  <a target="_blank" class="link-secondary" href="https://github.com/d3/d3-axis"
    >d3-axis</a
  >
  module and <CC code="select" /> from the
  <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-selection">d3-selection</a
  >
  module. Similar to the scales, the functions that you import construct the axis-function.
  However, in this case, you need to specify which scale to use when you construct
  the axis. Once you have constructed an axis, it is a function that takes a handle
  to the DOM element and fills that DOM element with the parts that build up the
  axis. Typically we add a specific <CC code="g" /> tag to use as DOM element for
  an axis, remember to apply a translation so that the content of the element is
  in the bottom margin of the SVG.
</p>
<p>
  Calling functions with a handle to a DOM element is different from calling
  normal functions, as those handles only exist when the DOM is constructed. In
  Svelte, you can use the <CC language="svelte" code="<g use:myFunction></g>" />
  syntax to specify an
  <a
    target="_blank"
    class="link-secondary"
    href="https://svelte.dev/docs#template-syntax-element-directives-use-action"
    >action</a
  >, i.e., a function that has to be called with the handle to an element as
  argument as soon as it becomes available. One final point, d3.js uses
  <em>selections</em> as handles to elements instead of raw HTML handles. So the
  axis-function has to be called with <CC
    language="javascript"
    code="xAxis(select(handle))"
  /> rather than <CC language="javascript" code="xAxis(handle)" />!
</p>
<p>
  Copy your answer from Exercise 3 to this exercise. Then, add an x-axis to the
  visualization. Also add a text label indicating that it is the x-axis, use the <em
    >currentcolor</em
  > for the label and center the label on the x-axis.
</p>
<Card><Ex_4 /></Card>

<h5>Exercise 5 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the file <CC
    code="src/routes/week_6/exercises/ex_5.svelte"
  />. Any changes you make to that file should show up below.
</p>
<p>
  For this exercise, the <CC code="values" /> array is changed into an array of objects.
  Each object within the array contains an x- and y-coordinate, and a category for
  coloring. Use the
  <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-scale">d3-scale</a
  >
  module again to plot both x- and y-values as circles to the DOM. Open the file
  and complete the exercise by defining linear scales for both the x- and y-coordinates
  of the circles. Use the <CC code="innerWidth" /> and <CC code="innerHeight" />
  variables as the outer coordinates of the scales' range. Now, use a categorical
  scale such as <CC language="javascript" code="scaleOrdinal()" /> to map de categories
  to the color values. You can use a predefined colorscale of the
  <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-scale-chromatic">d3-scale-chromatic</a
  >
  module for this (e.g. <CC code="schemeDark2" />). To map all different values
  of the 'category' key to the domain of your ordinal scale, you can use the <CC
    language="javascript"
    code="extent()"
  /> function from the
  <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-array">d3-array</a
  >
  module. This function operates on arrays, you can also specify an accessor function
  so the function knows which variable of the object it should look at. Next, plot
  the y-values about 10px above the circle elements and assign them the class <CC
    code="valueLabel"
  />.
</p>
<p>
  As a bonus: transform into a lollipop chart by adding svg line attributes from
  the bottom towards the circle elements.
</p>
<p>
  <em> Hint: Coordinates in SVG start in the upper-left corner!</em>
</p>
<Card><Ex_5 /></Card>

<h5>Exercise 6 (1 pt.)</h5>
<p>
  Create a barchart visualising the given fictive dataset with the number of
  viewers on popular streaming services.
</p>
<ol>
  <li>
    Import the necessary scales from the
    <a
      target="_blank"
      class="link-secondary"
      href="https://github.com/d3/d3-scale">d3-scale</a
    >
    module.
  </li>
  <li>Configure the x- and y-scales.</li>
  <li>
    Use svelte logic to loop through the data and create a rectangle for each
    entry.
  </li>
  <li>
    Add an axis to the left of the chart based on the scale you defined for the
    number of viewers.
  </li>
  <li>Lable the left axis</li>
  <li>
    Add an axis to the bottom of the chart based on the scale you defined for
    the streaming services.
  </li>
</ol>
<Card><Ex_6 /></Card>

<h5>Exercise 7 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the files <CC
    code="src/routes/week_6/exercises/ex_7.svelte"
  /> and <CC code="src/routes/week_6/exercises/_ex_7_scatterplot.svelte" />. Any
  changes you make to those file should show up below.
</p>
<p>
  Its time to combine all what you've learned so far and build a static version
  of the gapminder visualization by Hans Rosling. Use the data stored in <CC
    code="static/data/gapminder.json"
  />. The data includes an array of objects storing two keys in each object;
  'countries' and 'year'. For this exercise, you will need to extract the first
  object from the array (year 1800) and use the array stored in the 'countries'
  key as data to base your scatterplot visualization on.
</p>
<p>
  d3.js provides several functions in the
  <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-fetch">d3-fetch</a
  >
  module that load data. All of them folow the same structure. For example, to load
  a hypothetical file <CC code="myFile.csv" /> located in the <CC
    code="static"
  /> folder, you would use:
</p>
<Code
  language="javascript"
  code={`csv('/myFile.csv')
`}
/>
<p>
  Unfortunately, this function will not return the content of the file you are
  trying to load. Loading a file can take quite a lot of time, and your browser
  cannot afford to do nothing while it waits for the file to load. So,
  file-loading functions are <a
    target="_blank"
    class="link-secondary"
    href="https://www.w3schools.com/js/js_asynchronous.asp">asynchronous</a
  >. They do the actual loading in the background and give you a
  <em>promise</em> as return value instead. You can use this <em>promise</em> to
  access the content of the file when the loading has finished.
</p>
<p>
  Svelte also does not recommend loading data directly in the script of a
  component. Instead, data should be loaded the first time a component is
  actually shown on screen. Svelte provides the <CC
    language="javascript"
    code="onMount()"
  /> function for this purpose. It takes a function as argument and will run that
  function the first time your component is added to the DOM. There is also an <CC
    language="javascript"
    code="onDestroy()"
  /> function that you can use to clean up any dynamic resources you used.
</p>
<p>Combining both points, the code to load a file becomes:</p>
<Code
  language="javascript"
  code={`let data = null;
onMount(async () => {
  data = await csv('/myFile.csv');
});`}
/>
<p>
  Here, we initialize the data variable to <CC
    language="javascript"
    code="null"
  />, indicating that the data is not available yet. Then, in the <CC
    language="javascript"
    code="onMount()"
  /> function we define an asynchronous callback. Within that callback, we use the
  <CC language="javascript" code="await" /> keyword to obtain the content of the
  file from the <em>promise</em> returned by <CC
    language="javascript"
    code="csv()"
  /> funtion call. Essentially, <CC language="javascript" code="await" /> tells JavaScript
  to wait until the task of a <em>promise</em> is done and return the value that
  task created. Then, we assign the content of the file to the <CC
    code="data"
  /> variable. Note that we do not know <em>when</em> the data will be loaded, only
  that it will happen. So, in the markup of our component, we should only create
  the components that use the data when it is available!
</p>
<ol>
  <li>
    Import <CC code="_ex_7_scatterplot.svelte" /> in <CC code="ex_7.svelte" />.
  </li>
  <li>
    In <CC code="_ex_7_scatterplot.svelte" />, export a property called data to
    which you can provide the data in the <CC code="ex_7.svelte" /> component.
  </li>
  <li>
    Load the data in <CC code="ex_7.svelte" />, extract the countries for 1800
    and pass them on to <CC code="_ex_7_scatterplot.svelte" />. While the data
    is not loaded, you should show a message indicating the data is being
    loaded.
  </li>
  <li>
    Build the scatterplot based on the values 'income' and 'life_exp'. <em>
      Hint: when your data contains missing value (as is the case with this
      dataset), data loading functions interpret entire columns with missing
      values as textual instead of numerical. That usually leads to difficult to
      debug behaviour as it breaks our scales. You can use a trick to force
      JavaScript to treat values as a number in scales: <CC
        language="javascript"
        code="xScale(+d.value)"
      />.
    </em>
  </li>
  <li>Create an additional scale for the radius based on the 'population'.</li>
  <li>Color the circles based on the 'continent'.</li>
  <li>Add axes and labels.</li>
  <li>Add a title.</li>
  <li>Add a label for the year "1800" to the SVG.</li>
</ol>
<Card><Ex_7 /></Card>
